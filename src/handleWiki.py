import requests
import mwparserfromhell

URL = "https://regiowiki.at/w/api.php"

def removeHeading(my_str):
    return '\n'.join([elem.strip() for elem in my_str.split('\n')[1:]]) 

def get_section_wikitext_and_plaintext(title):
    
    sections_info = get_wiki_sections_info(title)
    
    sections_content = []
    for section in sections_info:
        section_title = section['line']
        section_index = section['index']
        
        # Hole den Wikitext des Abschnitts
        wikitext = get_wikitext(title, section=section_index)
        
        # Extrahiere Plaintext aus dem Wikitext
        plaintext = remove_images_and_get_plain_text(wikitext)
        
        sections_content.append({
            'title': section_title,
            'wikitext': wikitext[:500],  # Beispiel: Begrenze auf die ersten 500 Zeichen
            'plaintext': plaintext[:500]  # Beispiel: Begrenze auf die ersten 500 Zeichen
        })
    
    return sections_content

def get_wiki_sections_info(title):
    params = {
        "action": "parse",
        "page": title,
        "prop": "sections",
        "format": "json"
    }
    response = requests.get(URL, params=params)
    data = response.json()
    return data["parse"]["sections"] if "parse" in data else []

def get_wikitext(title, section=None):
    params = {
        "action": "query",
        "titles": title,
        "prop": "revisions",
        "rvprop": "content",
        "format": "json",
        "rvsection": section
    }
    response = requests.get(URL, params=params)
    data = response.json()
    page = next(iter(data["query"]["pages"].values()))
    return page["revisions"][0]["*"] if "revisions" in page else ""

def extract_plaintext(wikitext):
    wikicode = mwparserfromhell.parse(wikitext)
    return wikicode.strip_code().strip()

#Additional method to get Plain text with fully removed [[File:*]]-Links before
def remove_images_and_get_plain_text(wikitext):
    wikicode = mwparserfromhell.parse(wikitext)
    # Entferne alle Bild-Wiki-Links
    for node in wikicode.ifilter_wikilinks(recursive=True):
        if node.title.startswith("File:") or node.title.startswith("Datei:"):
            wikicode.remove(node)
    
    # Entferne alle verbleibenden Wiki-Markup-Elemente und erhalte den reinen Text
    plain_text = wikicode.strip_code().strip()
    
    return plain_text


def check_for_images_in_wikitext(wikitext):
    wikicode = mwparserfromhell.parse(wikitext)
    # Erhalte alle Wikilinks
    links = wikicode.filter_wikilinks()
    # Überprüfe, ob der Link eine Datei referenziert
    image_links = [link for link in links if link.title.startswith("Datei:") or link.title.startswith("File:")]

    has_images = len(image_links) > 0
    images_info = []

    for link in image_links:
        # Extrahiere den Titel, ersetze "Datei:" mit "File:"
        title = str(link.title).replace("Datei:", "File:")
        
        # Initialisiere die Beschreibung als None
        description = None
        
        # Versuche, den beschreibenden Text zu extrahieren, falls vorhanden
        if "|" in link.text:
            # Der beschreibende Text ist typischerweise der letzte Parameter nach einem "thumb" oder anderen Bildoptionen
            parts = link.text.split("|")
            if len(parts) > 1:  # Mehr als nur Titel und "thumb"
                # Der beschreibende Teil ist der letzte Textteil nach den Bildoptionen
                description = parts[-1].strip()

        images_info.append((title, description))

    return has_images, images_info



if __name__ == "__main__":
    
    sections_content = get_section_wikitext_and_plaintext("Benutzer:Mfchris84/Sandbox Mastodon")
    for section in sections_content:
        print(section['title'])
        print("Wikitext:", section['wikitext'])  # Zeige nur die ersten 100 Zeichen des Wikitextes
        print("Plaintext:", section['plaintext'][:500].replace("thumb","").replace(f"{section['title']}","").strip())  # Zeige nur die ersten 100 Zeichen des Plaintextes


        has_images, images_info = check_for_images_in_wikitext(section["wikitext"])
        if has_images:
            for title, description in images_info:
                print(f"Bildtitel: {title}, Beschreibung: {description}")
        else:
            print("Keine Bilder gefunden.")
        print("---")
    