from mastodon import Mastodon
from dotenv import load_dotenv
import os

class MastodonHandle:

    def __init__(self):
        load_dotenv()
        self.access_token = os.getenv('mastodon_token')
        self.mastodon_instance_url = os.getenv('mastodon_instance_url')
        self.myHandle = os.getenv('mastodon_handle')
        # Authenticate with the Mastodon API
        self.mastodon = Mastodon(
            access_token=self.access_token,
            api_base_url=self.mastodon_instance_url
        )
        accounts = self.mastodon.account_search(self.myHandle)

        # Retrieve the et your account ID
        if accounts:
            self.account_id = accounts[0]['id']
            #print("Your account ID is:", self.account_id)
        else:
            print("Account not found. Make sure your handle is correct.")

    def getStatuses(self):
        user_statuses = self.mastodon.account_statuses(self.account_id)
        # Print your posts
        for status in user_statuses:
            print(status['content'])

    def writeStatus(self, content):
        r = self.mastodon.status_post(content, visibility="unlisted", language="de")
        return r
    
    def deleteStatus(self, tootId):
        r = self.mastodon.status_delete(tootId)
        print(r)
    
if __name__ == '__main__':
    masto = MastodonHandle()
    #masto.getStatuses()
    #toot = masto.writeStatus("Das ist ein neuer Regiowiki.at Post, News aus der #Heimatkunde: https://regiowiki.at")
    #print(toot["id"])
    #tootId = toot["id"]
    #masto.mastodon.status_delete(toot["id"])
    

