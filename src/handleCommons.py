import requests
import urllib.request
import os
from dotenv import load_dotenv
load_dotenv()

def getFileUrl(file):
    headers = {
    'User-Agent': os.getenv("wiki_user"),
    'Content-type': 'application/json'
    }
    base_url = 'https://api.wikimedia.org/core/v1/commons/file/'
    url = base_url + file
    response = requests.get(url, headers=headers)
    return (response.json())

def downloadFile(commonsData):
    urllib.request.urlretrieve(commonsData["thumbnail"]["url"],f"files/{commonsData['title']}")

if __name__ == '__main__':
    commonsData = (getFileUrl("File:Bahnhof Horn (2024) ÖBB 5047 Regionalzüge am Mittelbahnsteig 02.jpg"))
    print(commonsData)

    print(f"https:{commonsData['file_description_url']}")
    print(commonsData["thumbnail"]["url"])

    
