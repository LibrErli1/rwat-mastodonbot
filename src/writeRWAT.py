import requests
from dotenv import load_dotenv
import os
from datetime import datetime



class writeWiki():
    
    def __init__(self):
        load_dotenv()
        self.user = os.getenv("wiki_botUser")
        self.userPass = os.getenv("wiki_botPass")
        self.URL = os.getenv("wiki_apiUrl")
        self.S = requests.Session()

        # Step 1: GET request to fetch login token
        PARAMS_0 = {
            "action": "query",
            "meta": "tokens",
            "type": "login",
            "format": "json"
        }

        R = self.S.get(url=self.URL, params=PARAMS_0)
        print(R)
        DATA = R.json()

        self.LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

        # Step 2: POST request to log in. Use of main account for login is not
        # supported. Obtain credentials via Special:BotPasswords
        # (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword
        PARAMS_1 = {
            "action": "login",
            "lgname": self.user,
            "lgpassword": self.userPass,
            "lgtoken": self.LOGIN_TOKEN,
            "format": "json"
        }

        R = self.S.post(self.URL, data=PARAMS_1)

        # Step 3: GET request to fetch CSRF token
        PARAMS_2 = {
            "action": "query",
            "meta": "tokens",
            "format": "json"
        }

        R = self.S.get(url=self.URL, params=PARAMS_2)
        DATA = R.json()

        self.CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

    def add_section_to_page(self, page, section_title, content, summary=""):    
        current_date = datetime.now()
        date_string = current_date.strftime('%Y-%m-%d')
        edit_params = {
            'action': "edit",
            'title': page,
            'section': "new",
            'text': content,
            'sectiontitle': f"{date_string}: {section_title}",
            'token': self.CSRF_TOKEN,
            'summary': summary,
            'format': "json"
        }
        r = self.S.post(self.URL, data=edit_params)
        return r.json()
    
    def dumpSourcePage(self, page, summary="Seite geleert, Inhalt vorher verschoben."):
        edit_params = {
            'action': "edit",
            'title': page,
            'text': "",
            'token': self.CSRF_TOKEN,
            'summary' : summary,
            'format': "json"
        }
        r = self.S.post(self.URL, data=edit_params)
        return r.json()

if __name__ == "__main__":
    rwat = writeWiki()
    print(f"CSRF: {rwat.CSRF_TOKEN}")
    print(f"LGN: {rwat.LOGIN_TOKEN}")

    targetPage="Benutzer:Mfchris84/Post auf Mastodon"
    sourcePage="Benutzer:Mfchris84/Post für Mastodon"
    sectionTitle = "Neuer Beitrag"
    wikiText = """Das ist ein #Testbeitrag der automatisch von der Seite https://regiowiki.at/wiki/Benutzer:Mfchris84/Post_für_Mastodon ausgelesen wird und auf dem Mastodon-Profil von Regiowki.At veröffentlicht wird. Er wird danach auf https://regiowiki.at/wiki/Benutzer:Mfchris84/Post_auf_Mastodon auf Mastodon "archiviert". Bis zu vier #Bilder sind dabei möglich, einfach hier angeben: [[Datei:Bahnhof Horn (2024) ÖBB 5047 Regionalzüge am Mittelbahnsteig 02.jpg|thumb]]"""
    rwat.add_section_to_page(targetPage, sectionTitle, wikiText)
    rwat.dumpSourcePage(sourcePage)