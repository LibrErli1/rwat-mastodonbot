import src.handleMastodon as mastodon
import src.handleCommons as commons
import src.handleWiki as wiki
import src.writeRWAT as writeWiki
import os

# Initialize Mastodon Methods
masto = mastodon.MastodonHandle()
mediaIds = []

# Initialize RWAT Writing Methods
rwat = writeWiki.writeWiki()
targetPage = "Regiowiki:Wikis.World/Post auf Mastodon"
sourcePage = "Regiowiki:Wikis.World/Post für Mastodon"

#Get Toot-Data from RWAT
sections_content = wiki.get_section_wikitext_and_plaintext(sourcePage)
for section in sections_content:
    print(section['title'])
    #Get Text of Section
    
    wikitext = wiki.removeHeading(section["wikitext"])
    #print("Wikitext: ", wikitext)
    
    tootContent = section['plaintext'][:500].replace("thumb","")
    tootContent = wiki.removeHeading(tootContent)
    tootContent = f"#NeuesVomRegiowikiAT: \n{tootContent}"
    print("Plaintext: ", tootContent)
    
    #Handle Images if given in WikiText
    has_images, images_info = wiki.check_for_images_in_wikitext(section["wikitext"])
    if has_images:
        #print("This section contains images:", image_titles)
        for image, description in images_info:
            #print(image)
            #Download Images
            commonsData = commons.getFileUrl(image)
            commons.downloadFile(commonsData)
            uploadedMedia = masto.mastodon.media_post(media_file=f"files/{commonsData['title']}", description=f"{description}\n\nQuelle: https://commons.wikimedia.org/wiki/{commonsData['title']}")
            mediaIds.append(uploadedMedia["id"])
            os.remove(f"files/{commonsData['title']}")


    #Post the Toot
    toot = masto.mastodon.status_post(tootContent, media_ids=mediaIds,language="de", visibility="public")
    rwat.add_section_to_page(targetPage, 
                            section_title= section["title"], 
                            content=f"{wikitext}\n\nGepostet auf Mastodon: [https://wikis.world/@RegiowikiAt/{toot['id']}]", 
                            summary=f"Toot {section['title']}  gepostet und ins Archiv geschoben.")
                                 
    print(f"Gepostet auf Mastodon: https://wikis.world/@RegiowikiAt/{toot['id']}")
    print("---")

#Dump the Source Page for Toots
rwat.dumpSourcePage(sourcePage)

