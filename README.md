# RWAT-Mastodon-Bot

This repository contains several scripts which allows a fully automatic Mastodon-Bot driven by a MediaWiki-Page. 

* The Bot extract a section of a defined `sourcePage` in a MediaWiki and creates a new Toot in your Mastodon-Profile (create the necessary writing keys for your Mastodon account)
* The Bot is able to upload up to four images to your toot, if they were added in your section on the sourcePage (the script expects that the images are stored on Wikimedia Commons). 
* The Bot dumps the sourcePage and moves the code to an archivePage on your MediaWiki with the URL to the posted toot.
* You can run this Bot as a cronjob on your dedicated server.

## Setup
* Clone this repository
* Create a `.env` file with your secrets and credentials
```
#Mastodon Secrets
mastodon_key=
mastodon_secret=
mastodon_token=
mastodon_handle={With Starting @}
mastodon_instance_url=https://

#MediaWiki (Bot-)User-Credentials
wiki_user=User:
wiki_botUser=User:
wiki_botPass=
wiki_apiUrl=
```
* Install the following necessary pip modules:
```python
pip install python-dotenv
pip install Mastodon.py
pip install mwparserfromhell
```

## Implementation
* Futher Information about usage (in German): [https://regiowiki.at/wiki/Regiowiki:Wikis.World]()

## Links, used respoitories

* **Mastodon.py** Doku: [https://mastodonpy.readthedocs.io/]()
* Mastodon-REST-API Doku: [https://docs.joinmastodon.org/api/guidelines/]()
